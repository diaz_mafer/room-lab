package com.example.mafer.roomlab;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mafer on 1/03/2018.
 */

public class Mascota {
        private String name;
        private String animal;
        private String age;
        private String color;

        public Mascota(String name, String animal, String age, String color){
            this.name = name;
            this.age = age;
            this.animal = animal;
            this.color = color;

        }

        public String getName(){
            return name;
        }

        public String getAnimal(){
            return animal;
        }

        public String getColor() {
            return color;
        }

        public String getAge(){
            return age;
        }



}
