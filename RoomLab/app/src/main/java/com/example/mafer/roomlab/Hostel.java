package com.example.mafer.roomlab;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Mafer on 1/03/2018.
 */

@Entity
public class Hostel {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "hostel_name")
    private String hostelName;


    @Embedded
    private Mascota mascota ;

    public Hostel(String hostelName, Mascota mascota, int id){
        this.hostelName = hostelName;
        this.mascota = mascota;
        this.id = id;
    }

    public int getId(){
        return id;
    }

    public String getHostelName(){
        return hostelName;
    }

    public Mascota getMascota(){
        return mascota;
    }

    @Override
    public String toString(){
        return " " +hostelName+ " " + mascota;
    }
}
