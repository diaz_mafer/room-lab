package com.example.mafer.roomlab;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;
import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity {

    private EditText name;
    private Button btnAdd;
    private TextView textView;
    private Mascota gato;

    private AppDataBase appDataBase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = findViewById(R.id.name);
        btnAdd = findViewById(R.id.btnAdd);
        gato = new Mascota("luna", "gato", "1" , "blanca");



        appDataBase = AppDataBase.getDatabase(this.getApplication());
        List<Hostel> hostelList = appDataBase.hostelDao().getAllHostels();


        if(hostelList != null && !hostelList.isEmpty()){
            Log.i("MainActivity", "list "+hostelList.toString());


        }



        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Hostel hostel = new Hostel(name.getText().toString(), gato, 0);
                appDataBase.hostelDao().addHostel(hostel);


            }
        });
    }
}
