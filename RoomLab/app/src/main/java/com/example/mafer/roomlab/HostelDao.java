package com.example.mafer.roomlab;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Mafer on 1/03/2018.
 */

@Dao
public interface HostelDao {
    @Insert(onConflict = REPLACE)
    void addHostel(Hostel hostel);

    @Delete
    void delete(Hostel hostel);

    @Query("SELECT * FROM Hostel")
    List<Hostel> getAllHostels();


}
